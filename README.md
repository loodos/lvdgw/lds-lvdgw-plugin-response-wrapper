# Response Wrapper Plugin - LVDGW 

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/blob/master/LICENSE)

[![dev build status](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/badges/dev/build.svg)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/commits/dev)
[![master build status](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/badges/master/build.svg)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/commits/master)

[![NuGet Pre Release](https://img.shields.io/nuget/v/LDS.LVDGW.Plugin.ResponseWrapper.svg)](https://www.nuget.org/packages/LDS.LVDGW.Plugin.ResponseWrapper/)
[![NuGet Pre Release](https://img.shields.io/nuget/vpre/LDS.LVDGW.Plugin.ResponseWrapper.svg)](https://www.nuget.org/packages/LDS.LVDGW.Plugin.ResponseWrapper/)

## What is it

Response Wrapper Plugin for LVDGW.

## Using the plugin

//TODO: write description

### Configuration

//TODO: write description

## Full Sample

//TODO: write description

## Who is using this

//TODO: add projects which is using this

## Commercial Support

//TODO: write description

## Contributing

//TODO: write description

## License

Loodos Vanilla Delivery Gateway (LVDGW) is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin-response-wrapper/blob/master/LICENSE)

## Acknowledgements

- [ASP.NET Core](https://github.com/aspnet)

Thanks for providing free open source licensing

- [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)
