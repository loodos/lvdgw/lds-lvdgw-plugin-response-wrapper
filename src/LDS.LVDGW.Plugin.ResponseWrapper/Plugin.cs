﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LDS.LVDGW.Plugin.ResponseWrapper
{
    public class Plugin : PluginBase, IResponseWrapperPlugin
    {
        //public override string Name => "ResponseWrapper";

        private string[] _wrappableContentTypes = new string[0];

        [ParameterInfo("WrappableContentTypes", "Wrappable Content Types", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "text/plain;application/json")]
        public string WrappableContentTypes {
            get => string.Join(";", _wrappableContentTypes);
            set => _wrappableContentTypes = value.Split(';', StringSplitOptions.RemoveEmptyEntries); 
        }

        [ParameterInfo("PropertyNameHttpStatus", "Property name for HttpStatus", Mandatory = false, ParameterType = ParameterTypes.Text, SampleValue = "HttpStatus")]
        public string PropertyNameHttpStatus { get; set; }

        [ParameterInfo("PropertyNameHttpStatusCode", "Property name for HttpStatusCode", Mandatory = false, ParameterType = ParameterTypes.Text, SampleValue = "Code")]
        public string PropertyNameHttpStatusCode { get; set; }

        [ParameterInfo("PropertyNameHttpStatusCodeDescription", "Property name for HttpStatusCode Description", Mandatory = false, ParameterType = ParameterTypes.Text, SampleValue = "Status")]
        public string PropertyNameHttpStatusCodeDescription { get; set; }

        [ParameterInfo("PropertyNameHttpStatusIsSuccess", "Property name for HttpStatus IsSuccess", Mandatory = false, ParameterType = ParameterTypes.Text, SampleValue = "Success")]
        public string PropertyNameHttpStatusIsSuccess { get; set; }

        [ParameterInfo("PropertyNameServerTime", "Property name for ServerTime", Mandatory = false, ParameterType = ParameterTypes.Text, SampleValue = "ServerTime")]
        public string PropertyNameServerTime { get; set; }

        [ParameterInfo("PropertyNameData", "Property name for Data", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "Data")]
        public string PropertyNameData { get; set; }
        

        public async Task<string> WrapResponseAsync(string responseBody, int statusCode, string contentType)
        {
            return await Task.Run(() =>
            {
                Log.Debug("WrapResponse statusCode:{statusCode} contentType:{contentType} responseBody:{responseBody}", statusCode, contentType, responseBody);
                
                // create wrapper response and convert to JSON
                return WrapResponse(responseBody, statusCode, contentType);
            });
        }

        private string WrapResponse(string responseBody, int statusCode, string contentType)
        {
            if (!IsResponseWrappable(contentType))
                return responseBody;

            var responseObject = new Dictionary<string, object>();

            if (IsResponseBodyJson(responseBody, contentType, out var jToken))
            {
                if (jToken is JArray)
                {
                    Log.Debug("responseBody is json array");

                    //lets convert responseBody to something we can use
                    var data = jToken.ToObject<List<object>>();

                    // set all responseBody to Data property
                    responseObject.Add(PropertyNameData, data);
                }
                else
                {
                    //lets convert responseBody to something we can use
                    var data = jToken.ToObject<Dictionary<string, object>>();

                    // responseBody contains Data property
                    if (data.ContainsKey(PropertyNameData))
                    {
                        responseObject.Add(PropertyNameData, data[PropertyNameData]);

                        // set other property (without Data property)
                        foreach (var key in data.Keys)
                        {
                            if (key == PropertyNameData)
                                continue;

                            responseObject.Add(key, data[key]);
                        }
                    }
                    // set all responseBody to Data property
                    else
                    {
                        responseObject.Add(PropertyNameData, data);
                    }
                }
            }
            else
            {
                Log.Debug("responseBody isn't json");

                // set all responseBody to Data property
                responseObject.Add(PropertyNameData, responseBody);
            }

            AddHttpStatusResponseObject(statusCode, ref responseObject);
            AddServerTimeResponseObject(ref responseObject);

            return JsonConvert.SerializeObject(responseObject);
        }

        private bool IsResponseWrappable(string contentType) => contentType != null &&
            _wrappableContentTypes.Any(x=> contentType.Contains(x, StringComparison.InvariantCultureIgnoreCase));

        private bool IsResponseBodyJson(string responseBody, string contentType, out JToken jToken)
        {
            bool isResponseBodyJson = false;
            jToken = null;

            if (contentType != null && contentType.Contains("application/json"))
            {
                if (!string.IsNullOrWhiteSpace(responseBody))
                {
                    responseBody = responseBody.Trim();

                    if ((responseBody.StartsWith("{") && responseBody.EndsWith("}")) || //For object
                    (responseBody.StartsWith("[") && responseBody.EndsWith("]"))) //For array
                    {
                        try
                        {
                            jToken = JToken.Parse(responseBody);
                            isResponseBodyJson = true;

                            Log.Debug("responseBody is json");
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, ex.ToString());
                        }
                    }
                }
            }

            return isResponseBodyJson;
        }

        private string GetStatusCodeDescription(int statusCode)
        {
            var statusCodeDescription = "";

            // https://github.com/dotnet/corefx/blob/bffef76f6af208e2042a2f27bc081ee908bb390b/src/System.Net.Primitives/src/System/Net/HttpStatusCode.cs
            if (Enum.IsDefined(typeof(HttpStatusCode), statusCode))
            {
                var httpStatusCode = (HttpStatusCode)Enum.ToObject(typeof(HttpStatusCode), statusCode);
                statusCodeDescription = httpStatusCode.ToString();
            }

            return statusCodeDescription;
        }

        private void AddHttpStatusResponseObject(int statusCode, ref Dictionary<string, object> responseObject)
        {
            if (!string.IsNullOrWhiteSpace(PropertyNameHttpStatus))
            {
                var httpStatus = new Dictionary<string, object>();

                if (!string.IsNullOrWhiteSpace(PropertyNameHttpStatusCode))
                    httpStatus.Add(PropertyNameHttpStatusCode, statusCode);

                if (!string.IsNullOrWhiteSpace(PropertyNameHttpStatusCodeDescription))
                    httpStatus.Add(PropertyNameHttpStatusCodeDescription, GetStatusCodeDescription(statusCode));

                if (!string.IsNullOrWhiteSpace(PropertyNameHttpStatusIsSuccess))
                    httpStatus.Add(PropertyNameHttpStatusIsSuccess, (statusCode >= 200 && statusCode < 300));

                responseObject.Add(PropertyNameHttpStatus, httpStatus);
            }
        }

        private void AddServerTimeResponseObject(ref Dictionary<string, object> responseObject)
        {
            if (!string.IsNullOrWhiteSpace(PropertyNameServerTime))
            {
                var serverTimeUTCInSeconds = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                responseObject.Add(PropertyNameServerTime, serverTimeUTCInSeconds);
            }
        }
    }
}
